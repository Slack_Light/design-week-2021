﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class HideFromMomUnityEvent : UnityEvent { }
public class HideFromMomEvent : MonoBehaviour
{
    public static HideFromMomUnityEvent hideFromMom = new HideFromMomUnityEvent();

    public void HideFromMom()
    {
        hideFromMom.Invoke();
    }
}

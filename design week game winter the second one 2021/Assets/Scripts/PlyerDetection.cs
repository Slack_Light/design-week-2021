﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;
using UnityEngine.Rendering.PostProcessing; 

public class PlyerDetection : MonoBehaviour
{
    FMOD.Studio.EventInstance Violins;
    FMOD.Studio.EventInstance Rumble;
    FMOD.Studio.EventInstance SummonMom;
    FMOD.Studio.EventInstance OminousMusic;
    FMOD.Studio.EventInstance CalmingMusic;
    FMOD.Studio.EventInstance Heartbeat;
    FMOD.Studio.EventInstance Clothing;

    [SerializeField]
    Transform mom;

    public enum State { Default, Seen, Searching, Chase, Safe }
    public State currentState;

    [SerializeField]
    List<GameObject> spottedEmployees = new List<GameObject>();

    float initialVolume;
    float volume;

    public PostProcessProfile profile;
    Grain filmGrain;
    LensDistortion zoom;
    ChromaticAberration colour;

    float seenTime; 
    private void Start()
    {
        Rumble = FMODUnity.RuntimeManager.CreateInstance("event:/Rumble");
        SummonMom = FMODUnity.RuntimeManager.CreateInstance("event:/SummonMom");
        Heartbeat = FMODUnity.RuntimeManager.CreateInstance("event:/MomApproaches");
        Clothing = FMODUnity.RuntimeManager.CreateInstance("event:/Clothing");
        OminousMusic = FMODUnity.RuntimeManager.CreateInstance("event:/ScaryMusic");
        CalmingMusic = FMODUnity.RuntimeManager.CreateInstance("event:/CalmMusic");
        Violins = FMODUnity.RuntimeManager.CreateInstance("event:/HuntingMom");

        currentState = State.Default;

        volume = 0;
        Heartbeat.start();
        Heartbeat.setPaused(true);
        profile.TryGetSettings(out filmGrain);
        filmGrain.intensity.value = 0;
        filmGrain.size.value = 0;
        if (profile.TryGetSettings(out zoom))
        {
            zoom.intensity.value = 0;

        }

        profile.TryGetSettings(out colour);
        colour.intensity.value = 0;


        OminousMusic.getVolume(out initialVolume);

        OminousMusic.start();
        CalmingMusic.start();
        CalmingMusic.setPaused(true);

        Violins.start();
        Violins.setPaused(true);

        Rumble.start();
        Rumble.setPaused(true);

        seenTime = 0;
    }
    
    private void OnDisable()
    {
        OminousMusic.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        CalmingMusic.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        Rumble.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        SummonMom.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        Heartbeat.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        zoom.intensity.value = 0;
        colour.intensity.value = 0;
        filmGrain.intensity.value = 0;
        filmGrain.size.value = 0;
        Violins.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
    }

    private void Update()
    {
        if(currentState == State.Default)
        {
            DefaultState();
        }  else if(currentState == State.Seen)
        {
            SeenState();
        }
        else if (currentState == State.Chase)
        {
            ChaseState();
        } else if (currentState == State.Searching)
        {
            SearchingState();
        } else if(currentState == State.Safe)
        {
            SafeState(); 
        }
    }

    void DefaultState()
    {
        Heartbeat.setPaused(true);
        Violins.setPaused(true);
        Rumble.setPaused(true);
        OminousMusic.setPaused(false);
        CalmingMusic.setPaused(true);
        zoom.intensity.value = 0;
        colour.intensity.value = 0;
        filmGrain.intensity.value = 0;
        filmGrain.size.value = 0;
    }

    void SeenState()
    {
        Rumble.setPaused(false);
        if (seenTime < 3.0f)
        {
            volume += 0.01f;
            seenTime += Time.deltaTime;
            Rumble.setVolume(volume);

            filmGrain.intensity.value = seenTime / 3;
            filmGrain.size.value = seenTime/ 3 * 1.6f;
        }
        else
        {
            Rumble.setPaused(true); 
            SummonMom.start();
            currentState = State.Chase;
            GetComponent<AlertMomEvent>().Alert();
        }
    }

    void ChaseState()
    {
        Heartbeat.setPaused(false);
        Violins.setPaused(false);
      
        float volume;
        volume = (150 - Vector3.Distance(mom.position, transform.position)) / 150;
        if (volume <= 0)
        {
            volume = 0.01f;
        }
        Violins.setVolume(volume);
        Heartbeat.setVolume(volume);

        zoom.intensity.value = Mathf.Clamp(-60 * ((150 - Vector3.Distance(mom.position, transform.position)) / 150), -60, 0);
        colour.intensity.value = 0.8f * ((150 - Vector3.Distance(mom.position, transform.position)) / 150);
        filmGrain.size.value = 1.6f;
        filmGrain.intensity.value = 1;
    }

    void SearchingState()
    {
        float volume;
        volume = (150 - Vector3.Distance(mom.position, transform.position)) / 150;
        Violins.setVolume(volume);
        Heartbeat.setVolume(volume);
        zoom.intensity.value = -60 * ((150 - Vector3.Distance(mom.position, transform.position)) / 150);
        colour.intensity.value = 0.8f * ((150 - Vector3.Distance(mom.position, transform.position)) / 150);
        filmGrain.intensity.value = (150 - Vector3.Distance(mom.position, transform.position)) / 150;
        filmGrain.size.value = 1.75f * ((150 - Vector3.Distance(mom.position, transform.position)) / 150);
    }

    void SafeState()
    {
        Heartbeat.setPaused(true);
        Violins.setPaused(true);
        Rumble.setPaused(true);
        OminousMusic.setPaused(true);
        CalmingMusic.setPaused(false); 
        zoom.intensity.value = 0;
        colour.intensity.value = 0;
        filmGrain.intensity.value = 0;
        filmGrain.size.value = 0;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == 10)
        {
            currentState = State.Safe;
            GetComponent<HideFromMomEvent>().HideFromMom();
            Clothing.start();
        } else if(other.gameObject.layer == 8)
        {
            if (!spottedEmployees.Contains(other.transform.parent.gameObject))
            {
                spottedEmployees.Add(other.transform.parent.gameObject);
            }
            if(currentState == State.Default)
            {
                currentState = State.Seen; 
            }
           
            if(spottedEmployees.Count == 1)
            {
                volume = 0f;
                seenTime = 0f;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.layer == 10)
        {
            currentState = State.Default;
            Clothing.start();
            OminousMusic.setPaused(false);
            CalmingMusic.setPaused(true);

            CheckIfSafe();
        } else if(other.gameObject.layer == 8)
        {
            if (spottedEmployees.Contains(other.transform.parent.gameObject))
            {
                spottedEmployees.Remove(other.transform.parent.gameObject);
            }

            if(spottedEmployees.Count <= 0)
            {
                Rumble.setPaused(true);
            }

            if (spottedEmployees.Count <= 0 && currentState == State.Seen)
            {
                currentState = State.Default;
            }
        }
    }

    public void SwitchToSearch()
    {
        if(currentState != State.Safe)
        {
            currentState = State.Searching;
        }
    }

    public void ReturnToNormal()
    {
        if(currentState != State.Safe)
        {
            currentState = State.Default;
        }
    }

    void CheckIfSafe()
    {
        if(spottedEmployees.Count > 0)
        {
            if (spottedEmployees.Contains(mom.gameObject))
            {
                currentState = State.Chase;
                GetComponent<AlertMomEvent>().Alert();
            }
            else
            {
                currentState = State.Seen;
            }
        }
    }
}

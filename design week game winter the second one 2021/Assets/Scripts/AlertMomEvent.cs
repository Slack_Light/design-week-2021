﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events; 

[System.Serializable]
public class AlertMomUnityEvent : UnityEvent {  }


public class AlertMomEvent : MonoBehaviour
{
    public static AlertMomUnityEvent alertMom = new AlertMomUnityEvent();

    public void Alert()
    {
        alertMom.Invoke(); 
    }
}



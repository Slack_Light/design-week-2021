﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollectionManager : MonoBehaviour
{[SerializeField] AudioClip collected;
   [SerializeField] List<GameObject> collectedItems = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        collected = gameObject.GetComponent<AudioSource>().clip = Resources.Load<AudioClip>("collected");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {

        
        if (other.tag == "Collectable")
        {
            collectedItems.Add(other.gameObject);

            //other.gameObject.GetComponentsInChildren<SkinnedMeshRenderer>().enabled = false;

            other.gameObject.GetComponent<BoxCollider>().enabled = false;
            other.gameObject.SetActive(false);
            gameObject.GetComponent<AudioSource>().PlayOneShot(collected, 0.6f);


            //collect
        }

        //check win condition on entering safe space
        if (other.gameObject.layer == 10)
        {
            int collected = 0;
            for (int i = 0; i < ListManager.objectiveList.Count; i++)
            {
                if (collectedItems.Contains(ListManager.objectiveList[i]))
                {
                    collected++;
                    //print("collected" + collectedItems[i].name);

                    if (collected >= ListManager.objectiveSize)
                    {
                        SceneManager.LoadScene(3);

                    }
                }

            }
            
        }
    }

}

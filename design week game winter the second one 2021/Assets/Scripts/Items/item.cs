﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class item : MonoBehaviour
{

    
    public bool isCollectable = false;
    [SerializeField]AudioClip collected;
    [SerializeField]public bool isCollected =false;

    // Start is called before the first frame update
    void Awake()
    {
        
            ListManager.itemTypes.Add(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        CollectableItemVisuals();
    }

    void CollectableItemVisuals()
    {
        if (!isCollectable)
            if (ListManager.objectiveList.Contains(gameObject))
            {
                isCollectable = true;
                this.tag = "Collectable";

                //add visuals here
                GetComponentInChildren<ParticleSystem>().enableEmission = true;

                //collected= gameObject.GetComponent<AudioSource>().clip = Resources.Load<AudioClip>("collected");


            }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (ListManager.objectiveList.Contains(gameObject))
            {

                //add pickup visuals here
                GetComponentInChildren<ParticleSystem>().enableEmission = false;

                //gameObject.GetComponentInChildren<MeshRenderer>().enabled = false;
                //gameObject.GetComponentInChildren<SkinnedMeshRenderer>().enabled = false;

                //gameObject.GetComponent<BoxCollider>().enabled = false;

            }
        }
    }


}

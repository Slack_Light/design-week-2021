﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ListManager : MonoBehaviour
{
    [SerializeField] Text listText;
    [SerializeField]static public int objectiveSize = 5;
    static public List<GameObject> objectiveList = new List<GameObject>();
    static public List<GameObject> itemTypes = new List<GameObject>();

    [SerializeField]GameObject checkIfOnList;

    // Start is called before the first frame update
    void Start()
    {
        objectiveList.Clear();
        objectiveSize = 5;
        fetchObjectives();
    }

    // Update is called once per frame
    void Update()
    {
        
        listText.text = (objectiveList[0].gameObject.name+"\n"+ objectiveList[1].gameObject.name+ "\n" + objectiveList[2].gameObject.name + "\n" + objectiveList[3].gameObject.name + "\n" + objectiveList[4].gameObject.name);
        for (int i = 0; i < objectiveList.Count; i++)
        {

            if (objectiveList[i].gameObject.activeSelf == false && objectiveList[i].gameObject.GetComponent<item>().isCollected == false)
            {
                objectiveList[i].name = objectiveList[i].name + " ✔";
                objectiveList[i].gameObject.GetComponent<item>().isCollected = true;
            }
        }


    }
   public void fetchObjectives()
    {
        if (objectiveList.Count < objectiveSize)
        {

            
            checkIfOnList = itemTypes[Random.Range(0, itemTypes.Count-1)];


            if (!objectiveList.Contains(checkIfOnList))
            {
                objectiveList.Add(checkIfOnList);
                fetchObjectives();
            }
            else
            {
                fetchObjectives();
            }
        }
    }
}

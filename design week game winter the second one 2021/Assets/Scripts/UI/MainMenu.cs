﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    // Start is called before the first frame update

    private void Start()
    {
        for (int i = 0; i < ListManager.itemTypes.Count; i++)
        {
            ListManager.itemTypes[i].gameObject.SetActive(true);
            ListManager.itemTypes[i].gameObject.GetComponent<BoxCollider>().enabled = true;
        }
        
        ListManager.objectiveList.Clear();
        ListManager.objectiveSize = 5;

    }
    public void PlayGame()
    {
        SceneManager.LoadScene("Game");
    }

    public void QuitGame()
    {
        Application.Quit();
    }

}

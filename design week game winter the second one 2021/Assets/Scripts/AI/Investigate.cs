﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;
using UnityEngine.AI; 

public class Investigate : StateBehaviour
{
    NavMeshAgent agent;

    Vision visionCone;
    TrackToTarget headMotion;

    Animator anim;

    private void OnEnable()
    {
        anim = GetComponentInChildren<Animator>();
        anim.Play("walk");
        headMotion = GetComponentInChildren<TrackToTarget>();
        headMotion.enabled = true; 
        GetComponent<AlertMomEvent>().Alert(); 
        agent = GetComponent<NavMeshAgent>();
        visionCone = GetComponentInChildren<Vision>();

        agent.enabled = true;
        agent.isStopped = false;
    }
    private void OnDisable()
    {
        headMotion.enabled = false; 
    }

    private void Update()
    {
        agent.destination = visionCone.lastKnownPosition; 
    }

    public void LoseSight()
    {
        if (isActiveAndEnabled)
        {
            StartCoroutine(Delay()); 
        }
    }

    IEnumerator Delay()
    {
        yield return new WaitForSeconds(4);
        blackboard.SendEvent("LoseInterest");
    }
}

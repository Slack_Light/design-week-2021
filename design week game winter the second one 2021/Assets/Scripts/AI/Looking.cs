﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;
using UnityEngine.AI; 

public class Looking : StateBehaviour
{
    Blackboard blackboard;
    NavMeshAgent agent;
    TrackToTarget headMotion;
    float detectionTime;
    Animator anim;

    float volume = 0.1f; 

    private void OnEnable()
    {
        anim = GetComponentInChildren<Animator>();
        anim.Play("Idle"); 
        blackboard = GetComponent<Blackboard>();
        detectionTime = blackboard.GetFloatVar("DetectionTime").Value; 
        agent = GetComponent<NavMeshAgent>(); 
        agent.isStopped = true;
        agent.enabled = false;

        headMotion = GetComponentInChildren<TrackToTarget>();
        headMotion.enabled = true;

        //StartCoroutine(Timer()); 

        IndicatePlayerSeen(); 
    }

    private void OnDisable()
    {
        volume = 0.1f;
        StopAllCoroutines(); 
        headMotion.enabled = false; 
    }

    private void LateUpdate()
    {
        //Calculate direction 
        Vector3 direction = GameManager.lastKnownPosition - transform.position;
        direction = new Vector3(direction.x, 0, direction.z);
        //Rotate to face movement direction
        Quaternion look = Quaternion.LookRotation(direction, Vector3.up);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, look, 50 * Time.deltaTime);
    }

    public void LoseSight()
    {
        if (isActiveAndEnabled)
        {
            print("lost player"); 
            blackboard.SendEvent("LoseSight"); 
        }
    }

    void IndicatePlayerSeen()
    {
        //Put any UI indications of the player being spotted here 
        print("saw player");
    }

    IEnumerator Timer()
    {
        yield return new WaitForSeconds(detectionTime);
        GetComponent<AlertMomEvent>().Alert();
    }
}

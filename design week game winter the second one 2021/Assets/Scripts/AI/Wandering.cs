﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BehaviourMachine;
using UnityEngine.AI;
using FMODUnity; 

public class Wandering : StateBehaviour
{
	Animator anim; 
	GameObject footSteps; 
	//AI wandering state 
	NavMeshAgent agent;
	Vector3 destination;
	Vector3 startLocation;

	Blackboard blackboard;
	FloatVar wanderRadius;

	bool setDestination;

    private void Awake()
    {
		startLocation = transform.position;
		anim = GetComponentInChildren<Animator>(); 
    }

    // Called when the state is enabled
    void OnEnable () {
		
		//Stop the navmesh
		agent = GetComponent<NavMeshAgent>();
		agent.enabled = true; 
		agent.isStopped = false;

		if(anim != null)
        {
			anim.Play("walk");
		}

		blackboard = GetComponent<Blackboard>();
		footSteps = blackboard.GetGameObjectVar("Footsteps").Value;
		wanderRadius = blackboard.GetFloatVar("WanderRadius");
		footSteps.SetActive(true);

		if (gameObject.tag == "Mom")
        {
			agent.speed = 5;
			AlertMomEvent.alertMom.AddListener(SpotPlayer);
		}

		setDestination = false; 
	}

    private void Update()
    {
		if (Vector3.Distance(transform.position, agent.destination) < 5f)
		{
			setDestination = false;
		}

		NavMeshPath validPath = new NavMeshPath();
		agent.CalculatePath(destination, validPath);
		if (validPath.status == NavMeshPathStatus.PathPartial)
		{
			setDestination = false;
		}

		while (!setDestination)
		{
			destination = startLocation + new Vector3(Random.insideUnitCircle.x, 0, Random.insideUnitCircle.y) * wanderRadius.Value;
			destination = new Vector3(destination.x, transform.position.y, destination.z);
			validPath = new NavMeshPath();
			agent.CalculatePath(destination, validPath);

			if (validPath.status != NavMeshPathStatus.PathPartial)
			{
				setDestination = true;
				agent.SetDestination(destination);
			}
		}
	}

    IEnumerator Navigating()
    {
		bool setDestination = false;
		//Pick a destination in the game space that is possible to navigate to and set the navmesh to navigate there
        while (!setDestination)
        {
			destination = startLocation + new Vector3(Random.insideUnitSphere.x, 0, Random.insideUnitSphere.z) * wanderRadius.Value;
			//destination = new Vector3(destination.x, transform.position.y, destination.z);
			NavMeshPath validPath = new NavMeshPath();
			agent.CalculatePath(destination, validPath);
			if (validPath.status != NavMeshPathStatus.PathPartial)
			{
				setDestination = true;
				agent.SetDestination(destination);
			}
			yield return null; 
		}

		//Once the distance to the destination is small 
		yield return new WaitUntil(() => Vector3.Distance (transform.position, destination) < 5);

		//Wait then start new path
		yield return null;

		StartCoroutine(Navigating()); 
    }

    public void SpotPlayer()
    {
        if (isActiveAndEnabled)
        {
			footSteps.SetActive(false); 
			StopAllCoroutines(); 
			blackboard.SendEvent("SpotPlayer"); 
        }
    }
}



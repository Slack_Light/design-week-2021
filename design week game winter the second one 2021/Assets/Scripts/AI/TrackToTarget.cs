﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine; 

public class TrackToTarget : MonoBehaviour
{
    Quaternion initialRotation;
    float startTime;

    private void OnEnable()
    {
        initialRotation = transform.rotation;
        startTime = 0; 
    }

    private void OnDisable()
    {
        //transform.rotation = initialRotation; 
    }

    private void LateUpdate()
    {
        startTime += Time.deltaTime; 

        //Calculate direction 
        Vector3 direction = GameManager.lastKnownPosition - transform.position;
        //Rotate to face movement direction
        Quaternion look = Quaternion.LookRotation(direction, Vector3.up);

        look = Quaternion.Slerp(initialRotation, look, startTime/2);

        Vector3 euler = look.eulerAngles;

        transform.eulerAngles = euler;
    }
}

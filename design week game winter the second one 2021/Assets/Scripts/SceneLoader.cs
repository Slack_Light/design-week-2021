﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 

public class SceneLoader : MonoBehaviour
{
    //This script loads scenes from menus

    [SerializeField]
    int sceneToLoad;

    private void Awake()
    {
        ListManager.itemTypes.Clear(); 
    }

    //Loads a desired scene
    public void LoadScene()
    {
        SceneManager.LoadScene(sceneToLoad);
    }

    //Closes the game
    public void Quit()
    {
        Application.Quit(); 
    }
}

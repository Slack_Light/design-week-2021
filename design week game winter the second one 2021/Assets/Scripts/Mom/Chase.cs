﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;
using UnityEngine.AI;

public class Chase : StateBehaviour
{
    NavMeshAgent agent;
    Blackboard blackboard;
    Transform playerPos;

    private void OnEnable()
    {
        blackboard = GetComponent<Blackboard>();
        playerPos = blackboard.GetGameObjectVar("Player").transform;

        agent = GetComponent<NavMeshAgent>();
        agent.speed = 10;

        HideFromMomEvent.hideFromMom.AddListener(StopChasing);

    }

    // Update is called once per frame
    void Update()
    {
        bool setDestination = false;

        while (!setDestination)
        {
            NavMeshPath validPath = new NavMeshPath();
            agent.CalculatePath(GameManager.lastKnownPosition, validPath);

            if (validPath.status != NavMeshPathStatus.PathPartial)
            {
                setDestination = true;
                agent.destination = GameManager.lastKnownPosition;
            }
            else
            {
                setDestination = true; 
            }
        }

        if (Vector3.Distance(transform.position, agent.destination) < 5)
        {
            playerPos.GetComponent<PlyerDetection>().SwitchToSearch();
            blackboard.SendEvent("StartInvestigation");
        }
    }

    void StopChasing()
    {
        blackboard.SendEvent("StopChasing");
    }
}

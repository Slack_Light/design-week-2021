﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviourMachine;
using UnityEngine.AI;

public class Searching : StateBehaviour
{
	//AI wandering state 
	NavMeshAgent agent;
	Vector3 destination;
	Vector3 startLocation;

	Blackboard blackboard;
	FloatVar wanderRadius;
	GameObjectVar playerCharacter;
	Transform playerPos;

	bool setDestination;

	private void Awake()
	{
		startLocation = transform.position;
	}

	// Called when the state is enabled
	void OnEnable()
	{
		//Stop the navmesh
		agent = GetComponent<NavMeshAgent>();
		agent.enabled = true;
		agent.isStopped = false;
		agent.speed = 10;

		blackboard = GetComponent<Blackboard>();
		playerCharacter = blackboard.GetGameObjectVar("Player");
		playerPos = playerCharacter.transform;
		HideFromMomEvent.hideFromMom.AddListener(StopChasing);

		setDestination = false;
		//Start navigation loop
		//StartCoroutine(Navigating());
		StartCoroutine(InvestigationTimer()); 
	}


	private void Update()
	{
		if (Vector3.Distance(transform.position, agent.destination) < 5f)
		{
			setDestination = false;
		}

		NavMeshPath validPath = new NavMeshPath();
		agent.CalculatePath(destination, validPath);
		if (validPath.status == NavMeshPathStatus.PathPartial)
		{
			setDestination = false;
		}

		while (!setDestination)
		{
			destination = GameManager.lastKnownPosition + new Vector3(Random.insideUnitCircle.x, 0, Random.insideUnitCircle.y) * 5;
			destination = new Vector3(destination.x, transform.position.y, destination.z);
			validPath = new NavMeshPath();
			agent.CalculatePath(destination, validPath);

			if (validPath.status != NavMeshPathStatus.PathPartial)
			{
				setDestination = true;
				agent.SetDestination(destination);
			}
		}
	}

	IEnumerator Navigating()
	{
		bool setDestination = false;
		//Pick a destination in the game space that is possible to navigate to and set the navmesh to navigate there
		while (!setDestination)
		{
			destination = GameManager.lastKnownPosition + new Vector3(Random.insideUnitCircle.x, 0, Random.insideUnitCircle.y)*5;
			NavMeshPath validPath = new NavMeshPath();
			agent.CalculatePath(destination, validPath);
			if (validPath.status != NavMeshPathStatus.PathPartial)
			{
				setDestination = true;
				agent.destination = destination;
			}
			yield return null;
		}

		//Once the distance to the destination is small 
		yield return new WaitUntil(() => Vector3.Distance(transform.position, destination) < 5);

		//Wait then start new path
		yield return new WaitForSeconds(2);

		StartCoroutine(Navigating());
	}

	IEnumerator InvestigationTimer()
    {
		yield return new WaitForSeconds(20);

		playerPos.GetComponent<PlyerDetection>().ReturnToNormal();
		blackboard.SendEvent("LoseInterest"); 
    }

	void StopChasing()
    {
		blackboard.SendEvent("LoseInterest");
	}
}
